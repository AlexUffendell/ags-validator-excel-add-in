# AGS Validator Excel Add-in

An Excel Add-in that utilises the command-line option of the AGS Python Library.  Features include checking, import and exporting from Excel.

Please log any bugs or feature requests as issues and I will work on adding them.  The main Addin file is not password protected so you can review and modify the code (and fix bugs please).  If you have additions to add to the Excel code please eport the module you have changed and upload the .bas or .frm files.  They will then be merged as part of a merge request.
